import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from './home/home.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import {MessageQComponent} from './message-q/message-q.component';

const routes: Routes = [
  {path:'',component: HomeComponent},
  {path:'mq',component:MessageQComponent},
  {path:'**',component:PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
