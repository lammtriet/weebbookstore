export function ErrorHandler(status:number){
  let message:string;
  if(status==0){
    message = "Connection error, please try again later";
    return message;
  }
  if(status==500){
    message = "Server error, please check server up";
    return message;
  }
  message = 'Try again later';
  return message;
}
