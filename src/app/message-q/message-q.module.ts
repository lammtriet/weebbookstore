import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatFormFieldModule,MatInputModule,MatSnackBarModule,MatTableModule } from '@angular/material';


import {MessageQComponent} from './message-q.component'
import {MQRoutingModule} from './message-q.routing.module'
import {MessageQService} from '../service/mq.service';

@NgModule({
  declarations: [
    MessageQComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MQRoutingModule,
  ],
  providers:[
    MessageQService,
  ]
})
export class MQModule { }
