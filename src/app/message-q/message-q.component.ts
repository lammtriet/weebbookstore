import { Component, OnInit } from '@angular/core';
import {MessageQService } from '../service/mq.service';
@Component({
  selector: 'app-message-q',
  templateUrl: './message-q.component.html',
  styleUrls: ['./message-q.component.scss']
})
export class MessageQComponent implements OnInit {

  constructor(private MQ:MessageQService) { }
  data;
  ngOnInit() {
  }
  Get(){
    this.MQ.invoke('https://books-api-production.herokuapp.com/api/v1/books', 'Get', null, null)
        .subscribe(
            result => {
              this.data = result;
              console.log(this.data);
            }
          );
  }
}
