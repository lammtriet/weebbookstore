import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MessageQComponent} from './message-q.component';

import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';

const routes: Routes = [
    {path:'mq',component: MessageQComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes),MatAutocompleteModule,
        MatFormFieldModule,],
  exports: [RouterModule],
  // entryComponents:[DetailComponent,CreateComponent,DeleteComponent,DelSComponent]
})
export class MQRoutingModule { }
