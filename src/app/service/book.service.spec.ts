import { TestBed, inject, fakeAsync  } from '@angular/core/testing';
import { HttpModule, XHRBackend, Response, ResponseOptions } from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import { BookService } from './book.service';

describe('BookService', () => {
  let service: BookService;
  let Mock: MockBackend;
  const uri="localhost:9000"
  const mockResponse = {
    data: [
      { title: 'UT',category:'drama',description:'utest' }
    ]}

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [BookService,{ provide: XHRBackend, useClass: MockBackend }]
    });
  });
  it('should be created', inject([BookService], (service: BookService) => {
    expect(service).toBeTruthy();
  }));


  it('should get books', inject([BookService, XHRBackend], (service, Mock) => {
    Mock.connections.subscribe(connection => {
      connection.mockRespond(new Response( new ResponseOptions({
        body: JSON.stringify(mockResponse)
      })));
    });
    service.getbooks().subscribe((res) => {
      expect(res).toBeDefined();
      expect(res).toEqual(mockResponse);
    });
  }));
  it('should get book', inject([BookService, XHRBackend], (service, Mock) => {
    Mock.connections.subscribe(connection => {
      connection.mockRespond(new Response( new ResponseOptions({
        body: JSON.stringify(mockResponse)
      })));
    });
    service.getbook(uri).subscribe((res) => {
      expect(res).toBeDefined();
      expect(res).toEqual(mockResponse);
    });
  }));
  it('should put book', inject([BookService, XHRBackend], (service, Mock) => {
    Mock.connections.subscribe(connection => {
      connection.mockRespond(new Response( new ResponseOptions({
        body: JSON.stringify(mockResponse)
      })));
    });
    service.putbook(uri,mockResponse).subscribe((res) => {
      expect(res).toBeDefined();
      expect(res).toEqual(mockResponse);
    });
  }));
  it('should post book', inject([BookService, XHRBackend], (service, Mock) => {
    Mock.connections.subscribe(connection => {
      connection.mockRespond(new Response( new ResponseOptions({
        body: JSON.stringify(mockResponse)
      })));
    });
    service.postbook(uri,mockResponse).subscribe((res) => {
      expect(res).toBeDefined();
      expect(res).toEqual(mockResponse);
    });
  }));
  it('should delete book and books', inject([BookService, XHRBackend], (service, Mock) => {
    Mock.connections.subscribe(connection => {
      connection.mockRespond(new Response( new ResponseOptions({
        body: JSON.stringify(mockResponse)
      })));
    });
    service.deletebooks(uri,mockResponse).subscribe((res) => {
      expect(res).toBeDefined();
      expect(res).toEqual(mockResponse);
    });
    service.deletebook(uri).subscribe((res) => {
      expect(res).toBeDefined();
      expect(res).toEqual(mockResponse);
    });
  }));
});
