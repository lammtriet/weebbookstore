import { AbstractControl } from '@angular/forms';
export function RequireMatch(control:AbstractControl) {
    let categories: string[] = ['comedy', 'drama', 'sport'];
    const selection: any = control.value;
    if (categories.indexOf(selection)==-1) {
        return { incorrect: true };
    }
    return null;
}

export function RequireMatchUpdate(control:AbstractControl) {
    let categories: string[] = ['comedy', 'drama', 'sport'];
    if(control.value){
      const selection: any = control.value;
      if (categories.indexOf(selection)==-1) {
        return { incorrect: true };
      }
      return null;
    }
}
