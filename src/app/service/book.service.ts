import { Injectable } from '@angular/core';
import {Http,Response,Headers,RequestOptions} from '@angular/http';



import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

import {IBook} from '../model/book-model';
import {Book} from '../model/book-model'

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor( private http:Http) { }

  getbooks() {
    return this.http.get('https://books-api-production.herokuapp.com/api/v1/books')
               .map(res=>res.json());
  }
  getbook(uri){
    let encodedUri = encodeURI(uri);
    let token =this.http.get(encodedUri).map(res=>res.json());
    return token
  }
  putbook(uri,object){
    let encodedUri = encodeURI(uri);
    return this.http.put(encodedUri,object).map(res=>res.json());

  }
  deletebook(uri){
    let encodedUri = encodeURI(uri);
    // console.log(uri);
    return this.http.delete(encodedUri).map(res=>res.json());;

  }
  deletebooks(uri,object){
    let encodedUri = encodeURI(uri);
    // console.log(uri);
    return this.http.post(encodedUri,object).map(res=>res.json());;

  }
  postbook(uri,object){
    let encodedUri = encodeURI(uri);
    return this.http.post(encodedUri,object).map(res=>res.json());

  }
}
