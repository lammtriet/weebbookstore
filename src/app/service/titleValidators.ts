import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { Observable, timer,Subscription } from 'rxjs';
import { map, switchMap  } from 'rxjs/operators';
// import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/map';
 const URL = 'https://books-api-production.herokuapp.com/api/v1/book/{title}?title=';


@Injectable({
  providedIn: 'root'
})
export class TitleValidators {
  constructor(private http: HttpClient) {}

  searchTitle(text) {
    // debounce
    return timer(1000)
      .pipe(
        switchMap(() => {
          let uri = URL + text;
          uri = encodeURI(uri);
          // Check if username is available
          return this.http.get<any>(uri)
        })
      );
  }

  titleValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.searchTitle(control.value)
        .pipe(
          map(
            (data) => {
              if(data){
                return { 'TitleExists': true}
              }
              return null;
            },
            (error)=>{
              return null;
            }
        )
      );
    };

  }

}
