import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatFormFieldModule,MatInputModule,MatSnackBarModule,MatTableModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
// import { FlexLayoutModule } from '@angular/flex-layout';

import {ListingComponent } from './listing/listing.component';
import {DetailComponent} from './detail/detail.component'
import {CreateComponent} from './create/create.component';
import {DeleteComponent} from './delete/delete.component';
import {DelSComponent} from './deletselected/delete.component';

import { BooksRoutingModule } from './book.routing.module';

import {BookService} from '../service/book.service';



@NgModule({
  declarations: [
    ListingComponent,
    DetailComponent,
    CreateComponent,
    DeleteComponent,
    DelSComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BooksRoutingModule,
    ReactiveFormsModule,
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatSnackBarModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCheckboxModule,
    // SelectionModelModule,
  ],
  providers:[
    BookService,
  ]
})
export class BooksModule { }
