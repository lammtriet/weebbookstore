import {By} from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatAutocompleteModule, MatFormFieldModule,MatInputModule,MatSnackBarModule,MatTableModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';

import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';

import {ListingComponent } from './listing/listing.component';
import {BookService} from '../service/book.service';
import { BooksRoutingModule } from './book.routing.module';
import {DetailComponent} from './detail/detail.component'
import {CreateComponent} from './create/create.component';
import {DeleteComponent} from './delete/delete.component';
import {DelSComponent} from './deletselected/delete.component';

describe('Books', () => {
      let component;
      let fixture;
      beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [ ListingComponent,
            DetailComponent,
            CreateComponent,
            DeleteComponent,
            DelSComponent ],
        imports: [
          BrowserAnimationsModule,
          MatDialogModule,
          MatTableModule,
          RouterTestingModule,
          MatFormFieldModule,
          BooksRoutingModule,
          MatInputModule,
          MatAutocompleteModule,
          MatFormFieldModule,
          MatSnackBarModule,
          MatPaginatorModule,
          MatSortModule,
          MatCheckboxModule,
          FormsModule,
          HttpModule,
          ReactiveFormsModule,
          HttpClientModule,
        ],
          providers:[
            BookService,
          ]
        }).compileComponents();
      }));
      beforeEach(() => {
        fixture = TestBed.createComponent(ListingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });
      it('should create', () => {
        expect(component).toBeTruthy();
      });
      it('Test table have 5 th and 5 columns ', () => {
            let tableCol = fixture.debugElement.nativeElement.querySelectorAll('th');
            // console.log(tableCol.length);
            expect(tableCol.length).toBe(5);
      });
      it('it should not have dialog open by default', function () {
          let dialogTitle = fixture.debugElement.nativeElement.querySelectorAll('mat-dialog-title');
          expect(dialogTitle.length).toBe(0);
      });
      // it('it should not have dialog open by default', function () {
          // let createPasteButton = fixture.debugElement.query(By.css("button"))
          // spyOn(component,"Create").and.callThrough();
          // createPasteButton.triggerEventHandler('click',null);
          // expect(component.Create).toHaveBeenCalled();
          // fixture.detectChanges();
          // let dialogTitle = fixture.debugElement.nativeElement.querySelectorAll('mat-dialog-title');
          // console.log(dialogTitle);
          // expect(dialogTitle.length).toBe(1);
      // });
});
