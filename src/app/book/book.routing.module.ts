import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListingComponent} from './listing/listing.component';
import {DetailComponent} from './detail/detail.component';
import {CreateComponent} from './create/create.component';
import {DeleteComponent} from './delete/delete.component';
import {DelSComponent} from './deletselected/delete.component';

import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';

const routes: Routes = [
    {path:'book',component: ListingComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes),MatAutocompleteModule,
        MatFormFieldModule,],
  exports: [RouterModule],
  entryComponents:[DetailComponent,CreateComponent,DeleteComponent,DelSComponent]
})
export class BooksRoutingModule { }
