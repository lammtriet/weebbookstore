import { Component,Inject } from '@angular/core';
import { FormControl,FormGroup,FormBuilder,FormArray,Validators  } from '@angular/forms';
import { ActivatedRoute,Router } from "@angular/router";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';
import { AbstractControl } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ErrorHandler} from '../../helper/ErrorHandler';

import {Book} from '../../model/book-model';
import {BookService} from '../../service/book.service';
import { RequireMatchUpdate} from '../../service/requireMatch';

export interface DialogData {
  title: string;
  category: ['drama','sport','comedy'];
  description: string;
}


@Component({
  selector: 'app-book',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent  {
  options: string[] = ['comedy', 'drama', 'sport'];
  BookDetailForm= new FormGroup({
    title: new FormControl(),
    category: new FormControl('',[RequireMatchUpdate]),
    description: new FormControl()
  });
  // updating=false;
  constructor(
  public dialogRef: MatDialogRef<DetailComponent>,
  @Inject(MAT_DIALOG_DATA) public data: DialogData,
  private NewService:BookService,private router:Router,
  private _snackBar: MatSnackBar) {}

  onNoClick(){
    this.dialogRef.close();
  }
  Update(){
    // this.updating=true;
    let token = this.data;
    if(this.BookDetailForm.value.category){token.category = this.BookDetailForm.value.category};
    if(this.BookDetailForm.value.description){token.description=this.BookDetailForm.value.description};
    let uri ='https://books-api-production.herokuapp.com/api/v1/book/';
    this.NewService.putbook(uri,token).subscribe(
      data =>{
        // this.updating=false;
        this.dialogRef.close(data);
        this._snackBar.open('Updated', 'Close', {
          duration: 2000,
        });
      },
      error =>{
        console.log('got error', error);
        let log = ErrorHandler(error.status);
        this._snackBar.open(log, 'Close', {
          duration: 2000,
        });
      }
    );
  }


}
