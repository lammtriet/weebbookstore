import { Component,Inject } from '@angular/core';
import { FormControl,FormGroup,FormBuilder,FormArray,Validators  } from '@angular/forms';
import { ActivatedRoute,Router } from "@angular/router";
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';
import { AbstractControl } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ErrorHandler} from '../../helper/ErrorHandler';

import {Book} from '../../model/book-model';
import {BookService} from '../../service/book.service';
import { RequireMatch} from '../../service/requireMatch';

export interface DialogData {
  title: string;
  category: ['drama','sport','comedy'];
  description: string;
  _id:string;
}


@Component({
  selector: 'app-book',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent  {
  options: string[] = ['comedy', 'drama', 'sport'];
  BookDetailForm= new FormGroup({
    title: new FormControl(),
    category: new FormControl('',[RequireMatch]),
    description: new FormControl()
  });
  constructor(
  public dialogRef: MatDialogRef<DeleteComponent>,
  @Inject(MAT_DIALOG_DATA) public data: DialogData,
  private NewService:BookService,private router:Router,
  private _snackBar: MatSnackBar) {}

  onNoClick(){
    this.dialogRef.close();
  }
  Delete(){
    // console.log(this.data);
    let uri ='https://books-api-production.herokuapp.com/api/v1/book/?_id='+this.data._id;
    // console.log(uri);
    this.NewService.deletebook(uri).subscribe(
      data =>{
        // console.log('done ', data);
        this.dialogRef.close(data);
        this._snackBar.open('Deleted', 'Close', {
          duration: 2000,
        });
      },
      error =>{
        // console.log('got error', error);
        let log = ErrorHandler(error.status);
        this._snackBar.open(log, 'Close', {
          duration: 2000,
        });
      }
    );
  }


}
