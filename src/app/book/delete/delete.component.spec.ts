import { TestBed, inject, async  } from '@angular/core/testing';
import {DeleteComponent} from './delete.component';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {By} from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule} from '@angular/material';

describe('BookService', () => {
  let component;
  let fixture;
  const dialogMock = {
    close: () => { }
   };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          DeleteComponent,],
      imports:[
          MatDialogModule,
          HttpModule,
          RouterTestingModule,
          MatSnackBarModule,
      ],
      providers: [
        // {provide: MatDialogTitle, useValue: {}},
        {provide: MatDialogRef, useValue: dialogMock},
        {provide: MAT_DIALOG_DATA, useValue: []}
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(DeleteComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  }));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('onNoClick click', () => {
    spyOn(component, 'onNoClick');
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#cancel');
    button.click();
    expect(component.onNoClick).toHaveBeenCalled();
  });
  it('Delete click', () => {
    spyOn(component, 'Delete');
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#delete');
    button.click();
    expect(component.Delete).toHaveBeenCalled();
  });
  it('dialog close in both click',()=>{
    let spy = spyOn(component.dialogRef, 'close').and.callThrough();
    component.onNoClick();
    expect(spy).toHaveBeenCalled();
    component.Delete();
    expect(spy).toHaveBeenCalled();
  });
})
