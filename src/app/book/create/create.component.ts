import { Component,Inject,OnInit } from '@angular/core';
import { FormControl,FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ActivatedRoute,Router } from "@angular/router";
import {MatSnackBar} from '@angular/material/snack-bar';
// import {MatTableDataSource} from '@angular/material/table';

import {IBook} from '../../model/book-model';
import {BookService} from '../../service/book.service';
import { RequireMatch} from '../../service/requireMatch';
import {TitleValidators} from '../../service/titleValidators';
import {ErrorHandler} from '../../helper/ErrorHandler';

import { MatAutocompleteModule, MatFormFieldModule } from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-book-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  options: string[] = ['comedy', 'drama', 'sport'];
  BookDetailForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<CreateComponent>,
    private NewService:BookService,private router:Router,
    private validator:TitleValidators,
    private fb:FormBuilder,
    // private errorHandler:ErrorHandler,
    private _snackBar: MatSnackBar) { }

  ngOnInit() {
    this.BookDetailForm= this.fb.group({
      title: new FormControl('',
      [Validators.required,Validators.maxLength(30)],
      this.validator.titleValidator()
    ),
    category: new FormControl('',
    [Validators.required,RequireMatch]),
    description: new FormControl('',
    [Validators.required])
  });

  }

  onNoClick(){
    this.dialogRef.close();
  }

  Create(){

    if (this.BookDetailForm.status =="INVALID") {
            return;
        }
    else{
      let token = this.BookDetailForm.value;
      let uri ='https://books-api-production.herokuapp.com/api/v1/book/';
      this.NewService.postbook(uri,token).subscribe(
        data =>{
          // let returndata = new MatTableDataSource<IBook>(data)
          this.dialogRef.close(data);
          this._snackBar.open('Created', 'Close', {
            duration: 2000,
          });
        },
        error =>{
          // console.log('got error', error.status);
          let log = ErrorHandler(error.status);
          this._snackBar.open(log, 'Close', {
            duration: 2000,
          });
        }
      );
    }
  }

}
