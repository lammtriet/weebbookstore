import { TestBed, inject, async  } from '@angular/core/testing';
import {CreateComponent} from './create.component';
import {MatDialogModule,MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatFormFieldModule,MatInputModule,MatSnackBarModule,MatTableModule } from '@angular/material';
import {By} from '@angular/platform-browser';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MatSnackBarModule} from '@angular/material';

describe('BookService', () => {
  let component;
  let fixture;
  const dialogMock = {
    close: () => { }
   };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          CreateComponent,],
      imports:[
          MatDialogModule,
          FormsModule,
          ReactiveFormsModule,
          HttpClientModule,
          BrowserAnimationsModule,
          HttpModule,
          RouterTestingModule,
          MatSnackBarModule,ReactiveFormsModule,FormsModule,
          MatAutocompleteModule, MatFormFieldModule,MatInputModule,MatSnackBarModule,MatTableModule
      ],
      providers: [
        // {provide: MatDialogTitle, useValue: {}},
        {provide: MatDialogRef, useValue: dialogMock},
        {provide: MAT_DIALOG_DATA, useValue: []}
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(CreateComponent);
    component = fixture.componentInstance;
    // fixture.detectChanges();
  }));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('onNoClick click', () => {
    spyOn(component, 'onNoClick');
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#cancel');
    button.click();
    expect(component.onNoClick).toHaveBeenCalled();
  });
  it('Create click', () => {
    spyOn(component, 'Create');
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#create');
    button.click();
    expect(component.Create).toHaveBeenCalled();
  });
})
