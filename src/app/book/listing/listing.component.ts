import { Component, OnInit, Inject, EventEmitter, Input,Output,HostListener, ViewChild,NgZone } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router } from "@angular/router";
import { FormControl,FormGroup,FormBuilder,FormArray,Validators  } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Sort,MatSort} from '@angular/material/sort';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTable} from '@angular/material';


import {BookService} from '../../service/book.service';
import { RequireMatchUpdate} from '../../service/requireMatch';

import {DetailComponent} from '../detail/detail.component';
import {IBook} from '../../model/book-model';
import {CreateComponent} from '../create/create.component';
import {DeleteComponent} from '../delete/delete.component';
import { DelSComponent} from '../deletselected/delete.component';
import {ErrorHandler} from '../../helper/ErrorHandler';


import {Observable, Subscription} from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-books',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})


export class ListingComponent implements OnInit {


  constructor(private NewService:BookService,public dialog: MatDialog, private router:Router,private _snackBar: MatSnackBar) { }
    dataSource:MatTableDataSource<IBook>;
    selection = new SelectionModel<IBook>(true, []);
    loaded=false;
    editing;
    newadded;
    displayedColumns: string[] = ['select','title', 'category', 'description','action'];
    @ViewChild(MatTable,{static:true}) table: MatTable<any>;
    @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;


    options: string[] = ['comedy', 'drama', 'sport'];
    BookDetailForm= new FormGroup({
      category: new FormControl('',[RequireMatchUpdate]),
      description: new FormControl()
    });

//init page

  ngOnInit() {
    this.getData();
  }
  getData(){
      this.NewService.getbooks().subscribe(
        (data) =>{
          this.dataSource = new MatTableDataSource<IBook>(data);
          this.dataSource.paginator=this.paginator;
          this.loaded=true;
        },
        (error)=>{
          let log = ErrorHandler(error.status);
          this._snackBar.open(log, 'Close', {
            duration: 2000,
          });
        }
      );
  }

//raload

  Reload(){
    this.loaded=false;
    let curp = this.dataSource.paginator['_pageIndex'];
    this.newadded='';
    this.getData();
    this.dataSource.paginator['_pageIndex']=curp;
    this.selection.clear();
  }





//sort


  sortData(sort: Sort) {
    if (sort.active && sort.direction !== '') {
      this.dataSource.data = this.dataSource.data.sort((a, b) => {
        const isAsc = sort.direction === 'asc';
        switch (sort.active) {
          case 'title': return this.compare(a.title.toLowerCase(), b.title.toLowerCase(), isAsc);
          case 'category': return this.compare(a.category, b.category, isAsc);
          default: return 0;
        }
      });
    }
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }


//search for title

  Search(SearchValue: string){
    this.dataSource.filter = SearchValue;
    this.dataSource.filterPredicate = (data, filter) =>
      (data.title.indexOf(filter) !== -1 );
    // console.log(this.dataSource);
  }


// selection

  isAllSelected() {
    if(this.loaded){
      const numSelected = this.selection.selected.length;
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }
  }

  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
        // console.log(this.selection);
  }

  checkboxLabel(row?: IBook): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;

    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'}`;
  }


//create

  Create() {
      const dialogRef = this.dialog.open(CreateComponent, {
      width: '70%'
    });
      dialogRef.afterClosed().subscribe(
        data => {
          this.getData();
          this.focus(data);
        },
        ()=>{setTimeout(function(){ this.newadded=null; }, 5000);}
      );
  }
  focus(bookdetail){
    if(bookdetail){
      this.newadded=bookdetail.title;
      let dtoken = this.dataSource.data.length;
      let siz=this.dataSource.paginator['_pageSize'];
      let pagepos = dtoken/siz;
      pagepos=Math.floor(pagepos);
      this.dataSource.paginator['_pageIndex']=pagepos;
    }
  }


//deletion

  Delete(bookdetail) {
    const dialogRef = this.dialog.open(DeleteComponent, {
      width: '70%',
      data: bookdetail
    });
    dialogRef.afterClosed().subscribe(
      data => {
        if(!data){
          // console.log('dont delete');
        }else{
          let dtoken = this.dataSource.data.indexOf(bookdetail);
          this.dataSource.data.splice(dtoken,1);
          this.dataSource._updateChangeSubscription();
          this.selection.clear();
        }
      },
    );
  }
  DeleteSelected(){
    if(this.selection['_selected'][0]){
      const dialogRef = this.dialog.open(DelSComponent, {
        width: '70%',
        data: this.selection['_selected']
      });
      dialogRef.afterClosed().subscribe(
        data => {
          if(!data){
            // console.log('dont delete');
          }else{
            let tokens = this.selection['_selected'];
            // console.log(tokens);
            for (let i = 0; i < tokens.length; i++){
              let dtoken = this.dataSource.data.indexOf(tokens[i]);
              this.dataSource.data.splice(dtoken,1);
            }
            this.dataSource._updateChangeSubscription();
            this.selection.clear();
          }
        },
      );
    }
    else{
      this._snackBar.open('Nothing selected', 'Close', {
        duration: 2000,
      });
    };
  }

//get total

  getTotal(){
    if(this.loaded){
      return this.dataSource.data.length;
    }
  }


//editing

  onRowEdit(bookdetail){
    this.editing = bookdetail;
  }
  onRowEditSave(){
    if(!this.BookDetailForm.value.category &&!this.BookDetailForm.value.description){
      this._snackBar.open('no input for update', 'Close', {
        duration: 2000,
      });
    }else{
      if(!this.BookDetailForm.controls.category.errors){
        if(this.BookDetailForm.value.category){this.editing.category = this.BookDetailForm.value.category};
        if(this.BookDetailForm.value.description){this.editing.description=this.BookDetailForm.value.description};
        let uri ='https://books-api-production.herokuapp.com/api/v1/book/';
        this.NewService.putbook(uri,this.editing).subscribe(
          data =>{
            this._snackBar.open('Updated', 'Close', {
              duration: 2000,
            });
          },
          error =>{
            let log = ErrorHandler(error.status);
            this._snackBar.open(log, 'Close', {
              duration: 2000,
            });
          }
        );
        this.editing = null;
        this.BookDetailForm.reset();
      }
    }
  }
  onRowEditCancel(){
    this.editing=null;
    this.BookDetailForm.reset();
  }
}
