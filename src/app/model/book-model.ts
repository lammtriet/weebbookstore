export class Book{
  _id:string;
  title:string;
  category:['drama','sport','comedy'];
  description:string;
}

export interface IBook{
  _id:string;
  title:string;
  category:string;
  description:string;
}
