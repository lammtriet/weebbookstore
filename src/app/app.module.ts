import { BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';

import {HttpClientModule} from '@angular/common/http';
import{HttpModule} from '@angular/http';
import{FormsModule} from '@angular/forms';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';

import { BooksModule } from './book/book.module';

import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {ListingComponent} from './book/listing/listing.component'
import {DetailComponent} from './book/detail/detail.component';
import { MessageQComponent } from './message-q/message-q.component'

import {MessageQService} from './service/mq.service';
import {MQModule} from './message-q/message-q.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BooksModule,
    MQModule,
    HttpModule,
    MatDialogModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
